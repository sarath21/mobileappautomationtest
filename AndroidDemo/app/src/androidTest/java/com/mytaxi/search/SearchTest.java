package com.mytaxi.search;

import android.Manifest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import com.mytaxi.android_demo.activities.MainActivity;
import com.mytaxi.helper.AccountFetchHelper;
import com.mytaxi.helper.page_objects.LoginPage;
import com.mytaxi.helper.page_objects.SearchPage;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Map;

import static android.support.test.espresso.action.ViewActions.click;
import static org.junit.Assert.assertNotNull;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class SearchTest {

    @Rule
    public GrantPermissionRule mRuntimePermissionRule
            = GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION);

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    private final String driverSearchText = "SA";
    private final String driverName = "Sarah Scott";

    @Before
    public void login() throws IOException {

        Map.Entry<String, String> userNamePassword = AccountFetchHelper.getRandomUser();

        assertNotNull(userNamePassword);

        LoginPage loginPage = new LoginPage(mActivityRule);
        assertTrue(loginPage.login(userNamePassword.getKey(), userNamePassword.getValue()));
    }


    @Test
    public void testSearchValidSearchText() {
        SearchPage searchPage = new SearchPage(mActivityRule);
        searchPage.search(driverSearchText, driverName).callDriver();
    }

}
