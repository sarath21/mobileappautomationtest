package com.mytaxi.helper.page_objects;

import android.support.test.rule.ActivityTestRule;

import com.mytaxi.android_demo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

public final class LoginPage {

    private ActivityTestRule<?> activityRule;

    public LoginPage(ActivityTestRule<?> activityRule) {
        this.activityRule  = activityRule;
    }

    public boolean login(String username, String password) {
        onView(withId(R.id.edt_username)).check(matches(isDisplayed()))
                .perform(typeText(username), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).check(matches(isDisplayed()))
                .perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).check(matches(isDisplayed()))
                .perform(click());
        return true;
    }
}
