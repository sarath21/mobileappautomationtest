package com.mytaxi.helper;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AccountFetchHelper {

    private AccountFetchHelper() {
    }

    private static final OkHttpClient CLIENT = new OkHttpClient();
    private static final Gson GSON = new Gson();
    private static final String URL = "https://randomuser.me/api/?seed=a1f30d446f820665";

    public static Map.Entry<String, String> getRandomUser() throws IOException {

        Request request = new Request.Builder()
                .url(URL)
                .build();
        Response response = CLIENT.newCall(request).execute();
        if(200 != response.code()) {
            Log.e("helper", "unable to fetch random account details - endpoint '" + URL + "' returned status code " + response.code());
            return null;
        }
        Map<String, Object> _response = GSON.fromJson(response.body().string(), Map.class);

        Log.d("helper", _response.toString());

        if(_response.get("results") != null) {
            List<Map<String, ?>> list = (List<Map<String, ?>>) _response.get("results");
            if(list != null && !list.isEmpty()) {
                Map<String, ?> loginDetails = (Map<String, ?> ) list.get(0).get("login");
                if(loginDetails != null && !loginDetails.isEmpty()) {
                    Map.Entry<String, String> userNamePassword = new AbstractMap.SimpleEntry<>( (String) loginDetails.get("username"), (String) loginDetails.get("password"));
                    Log.i("helper", "fetching random username password - " + userNamePassword);
                    return userNamePassword;
                }
            }
        }
        Log.e("helper", "unable to fetch random username password");
        return null;
    }
}
