package com.mytaxi.helper.page_objects;

import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.rule.ActivityTestRule;

import com.mytaxi.android_demo.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class SearchPage {

    private ActivityTestRule<?> activityRule;

    public SearchPage(ActivityTestRule<?> activityRule) {
        this.activityRule  = activityRule;
    }

    private void sleep(long millisec) {
        try {
            Thread.sleep(millisec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public SearchPage search(String driverSearchText, String driverName) {

        sleep(3000);

        onView(withId(R.id.textSearch)).check(matches(isDisplayed()))
                .check(matches(isCompletelyDisplayed()))
                .check(matches(isClickable()))
                .perform(typeText(driverSearchText), closeSoftKeyboard());

        onView(withId(R.id.searchContainer)).check(matches(isDisplayed()));

        onView(withText(driverName))
                .inRoot(RootMatchers.withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView()))))
                .perform(click());

        return this;
    }

    public SearchPage callDriver() {
        onView(withId(R.id.fab)).check(matches(isCompletelyDisplayed()))
                .perform(click());
        return this;
    }
}
